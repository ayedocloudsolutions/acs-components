

FROM python:slim-buster

ARG TERRAFORM_VERSION=0.14.11
ARG KUBE_VERSION=1.21.1
ARG HELM_VERSION=3.6.0

ENV ANSIBLE_PYTHON_INTERPRETER=/usr/local/bin/python

RUN apt-get update &&\
    apt-get -y --no-install-recommends install \
    ca-certificates=20200601~deb10u2 \
    curl=7.64.0-4+deb10u2 \
    git=1:2.20.1-2+deb10u3 \
    jq=1.5+dfsg-2+b1 \
    gettext=0.19.8.1-9 \
    python3-pip=18.1-5 \
    openssh-client=1:7.9p1-10+deb10u2 \
    sshpass=1.06-1 \
    unzip=6.0-23+deb10u2 \
    wget=1.20.1-1.1 && \
    rm -rf /var/lib/apt/lists/* &&\
    apt-get clean

WORKDIR /tmp

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
#RUN curl -LfsSO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && \
RUN wget -q -O "/usr/local/bin/kubectl" "https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/linux/amd64/kubectl" && \
    chmod +x /usr/local/bin/kubectl

RUN wget -q "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm
#chmod g+rwx /root && \

RUN wget -q "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin && \
    rm -rf /tmp/*

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir pip==21.1.3 cffi==1.14.6 && \
    pip install --no-cache-dir -r requirements.txt && \
    mkdir -p /ansible /etc/ansible /root/.acs && \
    echo 'localhost' > /etc/ansible/hosts

WORKDIR /ansible

COPY . /ansible

CMD [ "ansible-playbook", "--version" ]